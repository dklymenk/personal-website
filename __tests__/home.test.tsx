import {render, screen} from '@testing-library/react';
import Home from '../pages/[[...slug]]';

describe('Home', () => {
  it('renders employment history section', () => {
    render(<Home projects={[]} />);

    const employmentHistory = screen.getByRole('region', {
      name: /employment history/i,
    });

    expect(employmentHistory).toBeInTheDocument();
  });
});
