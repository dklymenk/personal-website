import {test, expect} from '@playwright/test';

test.describe('Projects', () => {
  test.beforeEach(async ({page}) => {
    await page.goto('/projects');
  });

  const title = 'Projects | Dmytro Klymenko';
  test(`title is "${title}"`, async ({page}) => {
    await expect(page).toHaveTitle(title);
  });
});
