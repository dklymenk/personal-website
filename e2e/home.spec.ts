import {test, expect} from '@playwright/test';

test.describe('home page', () => {
  test.beforeEach(async ({page}) => {
    await page.goto('/');
  });

  const heading = 'Dmytro Klymenko';
  test(`renders "${heading}" in an h1`, async ({page}) => {
    await expect(page.locator('h1')).toContainText(heading);
  });

  const title = 'Dmytro Klymenko';
  test(`title is "${title}"`, async ({page}) => {
    await expect(page).toHaveTitle(title);
  });

  test('matches snapshot', async ({page}) => {
    expect(await page.screenshot({fullPage: true})).toMatchSnapshot(
      'full-page.png'
    );
  });

  test.describe('project list', () => {
    test('renders all projects by default', async ({page}) => {
      const projects = page.locator('[data-test-id=project-list] > li');

      await expect(projects).toHaveCount(2);
    });

    test.describe('clicking on a tag', () => {
      test.beforeEach(async ({page}) => {
        await page.locator('svg:has-text("JavaScript") >> nth=0').click();
      });

      test('adds appropiate query param', async ({page}) => {
        await expect(page).toHaveURL('/js');
      });

      test('makes the tag button active', async ({page}) => {
        const jsButton = await page.locator(
          'a:has-text("JavaScript") >> nth=0'
        );

        await expect(jsButton).toHaveAttribute('aria-current', 'true');
      });

      test('matches snapshot', async ({page}) => {
        expect(
          await page.locator('a:has-text("JavaScript") >> nth=0').screenshot()
        ).toMatchSnapshot('selected-tag.png');
      });

      test('filters out the project without active tag', async ({page}) => {
        await expect(
          page.locator('text=test symfony project')
        ).not.toBeVisible();
      });

      test.describe('and clicking on it again', () => {
        test.beforeEach(async ({page}) => {
          await page.locator('svg:has-text("JavaScript") >> nth=0').click();
          await page.locator('h1').hover(); // Remove hover from the button
        });

        test('removes query param', async ({page}) => {
          await expect(page).toHaveURL('/');
        });

        test('makes the tag button inactive', async ({page}) => {
          const jsButton = await page.locator(
            'a:has-text("JavaScript") >> nth=0'
          );

          await expect(jsButton).toHaveAttribute('aria-current', 'false');
        });

        test('brings back the project without the tag', async ({page}) => {
          await expect(page.locator('text=test symfony project')).toBeVisible();
        });

        test('matches snapshot', async ({page}) => {
          expect(
            await page.locator('a:has-text("JavaScript") >> nth=0').screenshot()
          ).toMatchSnapshot('unselected-tag.png');
        });
      });
    });
  });
});
