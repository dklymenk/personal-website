/** @type {import('next').NextConfig} */
module.exports = {
  eslint: {
    dirs: ['pages', 'components', 'lib', 'src', 'data', 'e2e'],
  },
  reactStrictMode: true,
  // TODO Investigate why this is needed. Next.js tree shaking not working?
  webpack: (config, {isServer}) => {
    if (!isServer) {
      config.resolve.fallback.fs = false;
    }
    return config;
  },
};
