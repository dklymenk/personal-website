import {Icon, IconKey} from './icon';

const links: {
  href: string;
  iconKey: IconKey;
  label: string;
}[] = [
  {
    href: 'mailto:dmytro@klymenko.xyz',
    iconKey: 'mail',
    label: 'Email',
  },
  {
    href: 'https://gitlab.com/dklymenk',
    iconKey: 'gitlab',
    label: 'GitLab',
  },
  {
    href: 'https://github.com/dklymenk',
    iconKey: 'github',
    label: 'GitHub',
  },
];

export const ContactLinks = () => (
  <address className="flex justify-center space-x-4">
    {links.map(({href, iconKey, label}) => (
      <a key={href} href={href}>
        <Icon
          className="w-8 h-8 transition-colors hover:text-blue-700 dark:hover:text-blue-400"
          iconKey={iconKey}
          title={label}
        />
      </a>
    ))}
  </address>
);
