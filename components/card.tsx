import {ReactNode} from 'react';
import {twMerge} from 'tailwind-merge';

const getBaseClassName = () => {
  const className =
    'p-1 rounded-lg border bg-slate-50 border-black/5 dark:bg-slate-800/50 dark:border-white/5';
  return className;
};

export const Card = ({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) => {
  return (
    <div className={twMerge(`${getBaseClassName()} ${className}`)}>
      {children}
    </div>
  );
};

export const CardListItem = ({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) => {
  return (
    <li className={twMerge(`${getBaseClassName()} ${className}`)}>
      {children}
    </li>
  );
};
