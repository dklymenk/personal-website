import {SectionSeparator} from './';

export const Footer = () => (
  <>
    <SectionSeparator />

    <footer className="text-sm">
      <p className="mb-2">
        All trademarks, logos and brand names are the property of their
        respective owners.
      </p>
      <p>Copyright © 2021-2024 Dmytro Klymenko.</p>
    </footer>
  </>
);
