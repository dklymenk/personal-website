import Link from 'next/link';
import {Button} from './button';

export const LinkButton = ({
  children,
  href,
  active = false,
  scroll = true,
}: {
  children: React.ReactNode;
  href: React.ComponentProps<typeof Link>['href'];
  active?: boolean;
  scroll?: boolean;
}) => {
  return (
    <Link href={href} scroll={scroll}>
      <a aria-current={active}>
        <Button active={active}>{children}</Button>
      </a>
    </Link>
  );
};
