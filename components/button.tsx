export const Button = ({
  children,
  active = false,
}: {
  children: React.ReactNode;
  active?: boolean;
}) => {
  return (
    <button
      className={`transition-colors flex items-center p-1 rounded-lg ${
        active
          ? 'bg-blue-300 hover:bg-blue-400 dark:bg-blue-700 dark:hover:bg-blue-600'
          : 'bg-slate-200 hover:bg-slate-300 dark:bg-slate-700 dark:hover:bg-slate-600'
      }`}
    >
      {children}
    </button>
  );
};
