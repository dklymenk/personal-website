import {Project, TagKey, tags} from 'data';
import Image from 'next/image';
import {Tag} from './tag';
import {Card, CardListItem} from './card';
import {LinkButton} from './linkButton';

export const ProjectsList = ({
  activeTagKey,
  projects,
  pagePathname = '/',
}: {
  activeTagKey?: TagKey;
  projects: Project[];
  pagePathname?: string;
}) => {
  return (
    <>
      <Card className="flex flex-wrap gap-2 justify-center p-3">
        <LinkButton href={pagePathname} active={!activeTagKey}>
          <span className="m-0.5 text-base">All</span>
        </LinkButton>
        {(Object.keys(tags) as Array<TagKey>).map((key) => (
          <Tag
            pagePathname={pagePathname}
            active={activeTagKey === key}
            key={key}
            tag={tags[key]}
          />
        ))}
      </Card>
      <ul data-test-id="project-list">
        {projects.map((project) => (
          <CardListItem key={project.name} className="p-3 mt-4 space-y-5">
            <h3 className="text-xl">{project.name}</h3>
            <div className="flex flex-col gap-5 sm:flex-row-reverse sm:gap-0">
              {project.imageSrc && (
                <div className="flex flex-1 justify-center self-center sm:my-0">
                  <Image
                    src={project.imageSrc}
                    alt={project.name}
                    width="128"
                    height="128"
                    quality={100}
                    placeholder="blur"
                    className="rounded-3xl"
                  />
                </div>
              )}
              <div
                className="flex-[2_2_0%] space-y-5"
                dangerouslySetInnerHTML={{
                  __html: project.description,
                }}
              />
            </div>
            <div className="flex flex-wrap gap-2 justify-center sm:justify-start">
              {project.tags?.map((tag) => (
                <Tag
                  active={activeTagKey === tag.key}
                  key={tag.key}
                  tag={tag}
                />
              ))}
            </div>
          </CardListItem>
        ))}
      </ul>
    </>
  );
};
