import {ReactNode} from 'react';
import {Icon} from './icon';

export const Section = ({
  title,
  children,
  info,
}: {
  title: string;
  children?: ReactNode;
  info?: string;
}) => {
  return (
    <section aria-label={title} className="relative">
      <div className="mb-6 text-2xl">
        <h2 className="inline-block">{title}</h2>
        {info && (
          <span className="group">
            <span className="absolute left-2 invisible p-3 mt-8 w-5/6 text-base rounded-lg border opacity-0 transition-opacity duration-300 sm:w-auto sm:max-w-xs group-hover:visible group-hover:opacity-100 border-black/50 bg-slate-50 dark:border-white/50 dark:bg-slate-800">
              {info}
            </span>
            <Icon
              iconKey="info"
              className="inline ml-2 w-5 h-5 transition-colors duration-300 group-hover:text-blue-700 dark:group-hover:text-blue-400"
            />
          </span>
        )}
      </div>
      {children}
    </section>
  );
};
