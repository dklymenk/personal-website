import {render, screen} from '@testing-library/react';
import {Card, CardListItem} from './card';

describe('Card', () => {
  it('renders children', () => {
    render(
      <Card>
        <div data-testid="child"></div>
      </Card>
    );

    expect(screen.getByTestId('child')).toBeInTheDocument();
  });
});

describe('CardListItem', () => {
  it('renders children', () => {
    render(
      <CardListItem>
        <div data-testid="child"></div>
      </CardListItem>
    );

    expect(screen.getByTestId('child')).toBeInTheDocument();
  });

  it('renders a list item', () => {
    render(
      <CardListItem>
        <div></div>
      </CardListItem>
    );

    expect(screen.getByRole('listitem')).toBeInTheDocument();
  });
});
