import {CgInfo, CgMail} from 'react-icons/cg';
import {IconBaseProps} from 'react-icons/lib';
import {
  SiExpress,
  SiGithub,
  SiGitlab,
  SiJavascript,
  SiNestjs,
  SiNextdotjs,
  SiPhp,
  SiReact,
  SiSymfony,
  SiTypescript,
  SiUpwork,
} from 'react-icons/si';
import {TiHome} from 'react-icons/ti';
import {IoIosArrowForward} from 'react-icons/io';

const icons = {
  js: SiJavascript,
  php: SiPhp,
  react: SiReact,
  symfony: SiSymfony,
  github: SiGithub,
  gitlab: SiGitlab,
  upwork: SiUpwork,
  mail: CgMail,
  home: TiHome,
  arrowForward: IoIosArrowForward,
  ts: SiTypescript,
  nextjs: SiNextdotjs,
  nestjs: SiNestjs,
  info: CgInfo,
};

export type IconKey = keyof typeof icons;

interface IconProps extends IconBaseProps {
  iconKey: IconKey;
}

export const Icon = ({iconKey, ...props}: IconProps) => {
  const I = icons[iconKey];
  return <I role="img" {...props} />;
};
