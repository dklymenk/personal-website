import {render, screen} from '@testing-library/react';
import {SiJavascript, SiPhp} from 'react-icons/si';
import {Icon} from './icon';
import {renderToString} from 'react-dom/server';

describe('Icon', () => {
  it('renders js icon', async () => {
    render(<Icon iconKey="js" />);

    expect(screen.getByRole('img')).toContainHTML(
      renderToString(<SiJavascript />)
    );
  });

  it('renders php icon', async () => {
    render(<Icon iconKey="php" />);

    expect(screen.getByRole('img')).toContainHTML(renderToString(<SiPhp />));
  });

  it('passes className prop', async () => {
    render(<Icon iconKey="php" className="w-4 h-4" />);

    expect(screen.getByRole('img')).toContainHTML('class="w-4 h-4"');
  });
});
