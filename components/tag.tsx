import {Tag as TagType} from 'data';
import {Icon} from './icon';
import {LinkButton} from './linkButton';

export const Tag = ({
  tag,
  active = false,
  pagePathname = '/',
}: {
  tag: TagType;
  active?: boolean;
  pagePathname?: string;
}) => {
  return (
    <LinkButton
      href={
        active
          ? {pathname: pagePathname}
          : {pathname: `${pagePathname === '/' ? '' : pagePathname}/${tag.key}`}
      }
      active={active}
      scroll={false}
    >
      {tag.iconKey && (
        <Icon
          iconKey={tag.iconKey}
          title={tag.label}
          className="inline-block m-0.5 w-6 h-6"
        />
      )}
      {(!tag.compact || !Icon) && (
        <span className="m-0.5 text-base">{tag.label}</span>
      )}
    </LinkButton>
  );
};
