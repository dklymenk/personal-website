import {ReactNode} from 'react';
import {Footer} from './footer';

export const Layout = ({children}: {children: ReactNode}) => {
  return (
    <div className="min-h-screen bg-white border-gray-300 dark:bg-gray-900 text-slate-700 dark:text-slate-200">
      <div className="p-4 mx-auto max-w-5xl">
        {children}
        <Footer />
      </div>
    </div>
  );
};
