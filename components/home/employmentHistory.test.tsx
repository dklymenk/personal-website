import {render, screen, within} from '@testing-library/react';
import {EmploymentHistoryItem} from 'data/employementHistory';
import {EmploymentHistorySection} from './employmentHistorySection';

describe('EmploymentHistorySection', () => {
  it('renders employment history list', () => {
    render(<EmploymentHistorySection />);
    const employmentHistoryList = screen.getByRole('list');

    expect(employmentHistoryList).toBeInTheDocument();
  });

  it('renders dates as range', () => {
    const employmentHistory: EmploymentHistoryItem[] = [
      {
        title: 'Fault Management Engineer',
        employmentType: 'Full-time',
        companyName: 'lifecell Ukraine',
        startDate: 'Dec 2017',
        endDate: 'Jan 2020',
        location: 'Kyiv, Ukraine',
      },
    ];
    render(<EmploymentHistorySection employmentHistory={employmentHistory} />);

    expect(screen.getByText('Dec 2017 - Jan 2020')).toBeInTheDocument();
  });

  it('renders "Present" instead of endDate if not supplied', () => {
    const employmentHistory: EmploymentHistoryItem[] = [
      {
        title: 'Fault Management Engineer',
        employmentType: 'Full-time',
        companyName: 'lifecell Ukraine',
        startDate: 'Dec 2017',
        location: 'Kyiv, Ukraine',
      },
    ];
    render(<EmploymentHistorySection employmentHistory={employmentHistory} />);

    expect(screen.getByText('Dec 2017 - Present')).toBeInTheDocument();
  });

  it('renders the employement history items', () => {
    const employmentHistory: EmploymentHistoryItem[] = [
      {
        title: 'Full Stack Developer',
        employmentType: 'Full-time',
        companyName: 'Studio.gd',
        startDate: 'Jan 2020',
        location: 'Remote',
        description: 'did this, done that',
      },
      {
        title: 'Fault Management Engineer',
        employmentType: 'Full-time',
        companyName: 'lifecell Ukraine',
        startDate: 'Dec 2017',
        endDate: 'Jan 2020',
        location: 'Kyiv, Ukraine',
      },
    ];

    render(<EmploymentHistorySection employmentHistory={employmentHistory} />);
    const listItems = screen.getAllByRole('listitem');

    expect(listItems).toHaveLength(2);

    listItems.forEach((item, index) => {
      const {getByText} = within(item);
      const {
        title,
        employmentType,
        companyName,
        startDate,
        endDate,
        location,
        description,
      } = employmentHistory[index];

      expect(getByText(title)).toBeInTheDocument();
      expect(getByText(employmentType)).toBeInTheDocument();
      expect(getByText(companyName)).toBeInTheDocument();
      expect(item).toHaveTextContent(startDate);
      expect(getByText(location)).toBeInTheDocument();

      if (endDate) {
        expect(item).toHaveTextContent(endDate);
      }
      if (description) {
        expect(getByText(description)).toBeInTheDocument();
      }
    });
  });
});
