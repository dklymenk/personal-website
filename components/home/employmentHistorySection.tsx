import {CardListItem} from 'components/card';
import {Section} from 'components/section';
import {EmploymentHistoryItem} from 'data/employementHistory';

export const EmploymentHistorySection = ({
  employmentHistory,
}: {
  employmentHistory?: EmploymentHistoryItem[];
}) => {
  return (
    <Section title="Employment History">
      <ul className="space-y-4">
        {employmentHistory?.map(
          (
            {
              title,
              companyName,
              employmentType,
              startDate,
              endDate,
              location,
              description,
            },
            index
          ) => (
            <CardListItem className="p-3" key={index}>
              <h3 className="text-xl">{title}</h3>
              <div className="space-x-2">
                <span>{companyName}</span>
                <span>·</span>
                <span>{employmentType}</span>
              </div>
              <div className="opacity-80">
                {startDate} {`- ${endDate || 'Present'}`}
              </div>
              {location && <div className="opacity-80">{location}</div>}
              <div>{description}</div>
            </CardListItem>
          )
        )}
      </ul>
    </Section>
  );
};
