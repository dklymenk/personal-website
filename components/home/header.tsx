import {ContactLinks} from 'components/contactLinks';
import {SectionSeparator} from 'components/sectionSeparator';
import {tagline} from 'data';

export const Header = () => (
  <>
    <header className="space-y-4 sm:flex sm:space-y-0">
      <div className="flex-[2_2_0%] space-y-4">
        <h1 className="text-3xl font-bold text-center">Dmytro Klymenko</h1>
        <p className="text-center">{tagline}</p>
      </div>
      <div className="flex-1 self-center">
        <ContactLinks />
      </div>
    </header>

    <SectionSeparator />
  </>
);
