import {LinkButton} from 'components/linkButton';
import {ProjectsList} from 'components/projectsList';
import {Section} from 'components/section';
import {Project, projectsDisclaimer, TagKey, tags} from 'data';

export const ProjectsSection = ({
  activeTagKey,
  projects,
  totalProjects,
}: {
  activeTagKey?: TagKey;
  projects: Project[];
  totalProjects: number;
}) => {
  return (
    <Section title="Projects" info={projectsDisclaimer}>
      <ProjectsList activeTagKey={activeTagKey} projects={projects} />
      {totalProjects > projects.length && (
        <div className="flex flex-wrap gap-2 justify-center my-4">
          <div className="leading-9 text-center">
            Showing {projects.length} out of {totalProjects}{' '}
            {activeTagKey ? tags[activeTagKey].label : ''} projects
          </div>
          <LinkButton href={`/projects/${activeTagKey || ''}`}>
            <span className="m-0.5 text-base">View all</span>
          </LinkButton>
        </div>
      )}
    </Section>
  );
};
