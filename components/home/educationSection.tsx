import {CardListItem} from 'components/card';
import {Section} from 'components/section';
import {EducationItem} from 'data';

export const EducationSection = ({
  education,
}: {
  education?: EducationItem[];
}) => {
  return (
    <Section title="Education">
      <ul className="space-y-4">
        {education?.map(
          (
            {school, degree, fieldOfStudy, disseration, startDate, endDate},
            index
          ) => (
            <CardListItem className="p-3" key={index}>
              <h3 className="text-xl">{school}</h3>
              <div>
                {degree}, {fieldOfStudy}
              </div>
              {disseration && (
                <div className="opacity-80">Disseration: {disseration}</div>
              )}
              <div className="opacity-80">
                {startDate} - {endDate}
              </div>
            </CardListItem>
          )
        )}
      </ul>
    </Section>
  );
};
