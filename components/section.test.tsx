import {render, screen} from '@testing-library/react';
import {Section} from './section';

describe('Section', () => {
  it('renders title', () => {
    render(<Section title="Section Title" />);

    expect(screen.getByRole('heading')).toHaveTextContent('Section Title');
  });

  it('renders children', () => {
    render(
      <Section title="Title">
        <div data-testid="child"></div>
      </Section>
    );

    expect(screen.getByTestId('child')).toBeInTheDocument();
  });
});
