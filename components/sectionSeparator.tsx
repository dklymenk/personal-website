export const SectionSeparator = () => {
  return <hr className="my-8 h-px bg-black border-none dark:bg-white" />;
};
