import {ContactLinks} from 'components/contactLinks';
import {Icon} from 'components/icon';
import {SectionSeparator} from 'components/sectionSeparator';
import {projectsDisclaimer} from 'data';
import Link from 'next/link';

export const Header = () => (
  <>
    <header className="relative space-y-4 sm:flex sm:space-y-0">
      <div className="flex-[2_2_0%] w-full space-y-4 text-center ">
        <div className="space-y-4 sm:flex sm:justify-center sm:space-y-0">
          <nav
            className="text-sm sm:absolute sm:left-0 sm:flex-none"
            aria-label="Breadcrumb"
          >
            <Link href="/">
              <a className="">
                <Icon
                  className="inline-block mb-0.5 w-4 h-4 transition-colors hover:text-blue-700 dark:hover:text-blue-400"
                  iconKey="home"
                />
              </a>
            </Link>
            <Icon
              className="inline-block mb-0.5 w-4 h-4"
              iconKey="arrowForward"
            />
            <Link href="/projects">
              <a
                aria-current="page"
                className="transition-colors hover:text-blue-700 dark:hover:text-blue-400"
              >
                Projects
              </a>
            </Link>
          </nav>
          <h1 className="text-3xl font-bold sm:flex-auto">Projects</h1>
        </div>
        <p>{projectsDisclaimer}</p>
      </div>
      <div className="flex-1 self-center">
        <ContactLinks />
      </div>
    </header>

    <SectionSeparator />
  </>
);
