export interface EmploymentHistoryItem {
  title: string;
  description?: string;
  employmentType: string;
  companyName: string;
  startDate: string;
  endDate?: string;
  location?: string;
}

export const employmentHistory: EmploymentHistoryItem[] = [
  {
    title: 'Full-Stack Software Engineer (Remote)',
    employmentType: 'Permanent Full-time',
    companyName: 'Syniti',
    startDate: 'Feb 2023',
    location: 'Canada',
  },
  {
    title: 'Full-Stack Developer (Remote)',
    employmentType: 'Freelance',
    companyName: 'Studio.gd',
    startDate: 'Sep 2019',
    endDate: 'Feb 2023',
    location: 'Pantin, Île-de-France, France',
  },
  {
    title: 'Full-Stack Developer (Remote)',
    employmentType: 'Freelance',
    companyName: 'BioFormula Select',
    startDate: 'Aug 2021',
    endDate: 'Jul 2022',
  },
  {
    title: 'React Native Developer (Remote)',
    employmentType: 'Freelance',
    companyName: 'Expensify',
    startDate: 'May 2021',
    endDate: 'Jan 2022',
    location: 'San Francisco, California, United States',
  },
  {
    title: 'Fault Management Engineer',
    employmentType: 'Full-time',
    companyName: 'lifecell Ukraine',
    startDate: 'Dec 2017',
    endDate: 'Jan 2020',
    location: 'Kyiv, Kyiv City, Ukraine',
  },
];
