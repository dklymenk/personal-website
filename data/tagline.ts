export const tagline =
  'A full-stack developer with over 5 years of commercial experience in designing, building, testing, debugging, deploying and maintaining web and mobile applications.';
