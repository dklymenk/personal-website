export interface EducationItem {
  school: string;
  degree: string;
  fieldOfStudy: string;
  disseration?: string;
  startDate: string;
  endDate: string;
}

export const education: EducationItem[] = [
  {
    school:
      "National Technical University of Ukraine 'Kyiv Polytechnic Institute'",
    degree: "Master's degree",
    fieldOfStudy: 'Automation and Computer-Integrated Technologies',
    disseration:
      'Automated Product Quality Control Using an Image Classifier Based on a Neural Network',
    startDate: '2018',
    endDate: '2019',
  },
  {
    school:
      "National Technical University of Ukraine 'Kyiv Polytechnic Institute'",
    degree: "Bachelor's degree",
    fieldOfStudy: 'Automation and Computer-Integrated Technologies',
    startDate: '2014',
    endDate: '2018',
  },
];
