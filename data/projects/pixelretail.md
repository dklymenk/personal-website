A full-stack digital signage system for small and mid-sized businesses operating
in France with a convenient web dashboard for managing content in real time.

The main components of the system are:

- Symfony-powered back end with a REST API and a messaging service to send
  updates to the client applications
- dashboard built with React for managing slideshows, products, events and other
  content
- slideshow app built with React to display the content on the screens
- Node.js service running on Raspberry Pi devices in the stores that is
  responsible for managing the slideshow instances running in Chromium browser,
  interacting with queue system peripherals (thermal printers, speakers, etc.)
  and listening for updates from the back end

The system was developed by a team at [Studio.gd](https://studio.gd/). My role
in the project was to lead the development efforts on the back end and Raspberry
Pi devices. At one point I was leading a team of three developers. In addition
to the development work I was also assisting with technical evaluation of
potential hires as well as mentoring of junior developers.

The project was discontinued in 2023, so the website is no longer available.
Still, I'm proud of the work we did and the experience I gained from it. To
this day I attribute most of my growth as an engineer to PixelRetail.
