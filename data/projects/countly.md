[Countly](https://count.ly/) is a typical analytics platform that provides
functionality to collect and analyse user activity on a website or an
application. In addition to displaying user metrics the dashboard also allows
you to manage push notifications.

I used it on one of the projects to send push notifications to users and one of
the SDK's APIs was broken. I fixed the issue and submitted a pull request to the
upstream repository.

[Contributions](https://github.com/Countly/countly-sdk-react-native-bridge/pulls?q=is%3Apr+author%3Adklymenk+is%3Aclosed+)
