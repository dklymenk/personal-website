import fs from 'fs';
import path from 'path';
import {remark} from 'remark';
import remarkRehype from 'remark-rehype';
import rehypeStringify from 'rehype-stringify';
import addClasses from 'rehype-add-classes';
import {Tag, tags} from '../tags';
import {classes} from './classes';
import PixelRetailImage from 'public/images/projects/pixelretail.png';
import ExpensifyImage from 'public/images/projects/expensify.png';
import WarcrimeImage from 'public/images/projects/warcrime.png';
import CountlyImage from 'public/images/projects/countly.png';
import LunarvimImage from 'public/images/projects/lunarvim.png';
import {StaticImageData} from 'next/image';

export interface Project {
  name: string;
  tags?: Tag[];
  description: string;
  imageSrc?: StaticImageData;
}

const getProjectDescription = async (name: string) => {
  const fullPath = path.join(process.cwd(), 'data', 'projects', `${name}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');

  const processedContent = await remark()
    .use(remarkRehype)
    .use(addClasses, classes)
    .use(rehypeStringify)
    .process(fileContents);
  const contentHtml = processedContent.toString();

  return contentHtml;
};

export const getProjects = async (): Promise<Project[]> =>
  process.env.NEXT_PUBLIC_ENV === 'test'
    ? [
        {
          name: 'test symfony project',
          tags: [tags.php, tags.symfony],
          description: '',
        },
        {
          name: 'test full stack project',
          tags: [tags.js, tags.php, tags.symfony],
          description: '',
        },
      ]
    : [
        {
          name: 'Warcrime',
          tags: [
            tags.js,
            tags.ts,
            tags.react,
            tags.rn,
            tags.nestjs,
            tags.volunteering,
          ],
          description: await getProjectDescription('warcrime'),
          imageSrc: WarcrimeImage,
        },
        {
          name: 'Expensify',
          tags: [tags.js, tags.rn, tags.react, tags.commercial],
          description: await getProjectDescription('expensify'),
          imageSrc: ExpensifyImage,
        },
        {
          name: 'Shared Password Store Manager',
          tags: [tags.sh, tags.personal],
          description: await getProjectDescription(
            'shared-password-store-manager'
          ),
        },
        {
          name: 'PixelRetail',
          tags: [tags.php, tags.js, tags.symfony, tags.react, tags.commercial],
          description: await getProjectDescription('pixelretail'),
          imageSrc: PixelRetailImage,
        },
        {
          name: 'Personal Website',
          tags: [tags.js, tags.ts, tags.nextjs, tags.react, tags.personal],
          description: await getProjectDescription('personal-website'),
        },
        {
          name: 'Countly',
          tags: [tags.rn, tags.volunteering],
          description: await getProjectDescription('countly'),
          imageSrc: CountlyImage,
        },
        {
          name: 'Open Exchange Rates',
          tags: [tags.php, tags.volunteering],
          description: await getProjectDescription('open-exchange-rates'),
        },
        {
          name: 'LunarVim',
          tags: [tags.volunteering],
          description: await getProjectDescription('lunarvim'),
          imageSrc: LunarvimImage,
        },
      ];

export const projectsDisclaimer =
  'This is some of the work I can publicly display my involvement in. The list mostly consists of open-source software contributions and my personal projects, but there are a few commercial ones here as well.';
