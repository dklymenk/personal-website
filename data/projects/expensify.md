[Expensify](https://www.expensify.com/) is a software company that develops an
expense management system for personal and business use.

In 2020 they starting working on a new open-source version of their core
application. A chat-based cross-platform app that provides functionality such as
invoicing, bill payments and much more.

The company keeps a list of open issues on their GitHub and through Upwork hires
contributors to work on those issues. It is through that platform that I started
working on the project. Over the course of nine months more than 20 pull
requests of mine were merged into the main branch.

[Contributions](https://github.com/Expensify/App/pulls?q=is%3Apr+author%3Adklymenk+is%3Aclosed+)
