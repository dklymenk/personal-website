This is the very same website you are currently reading. Practically, it is just
a static site, which makes it fast and simple, but, being built with Next.js, it
offers great user experience and is easy to maintain.

As an additional challenge I wanted all of the website's functionality to be
available for users not running JavaScript in their browsers. While Next.js is a great tool for building
SEO-friendly websites, it does not help much with designing JavaScript-less
ones.

Either way, despite some setbacks, the website is fully functional without
JavaScript.

I use [Playwright](https://playwright.dev/) to test the website's functionality
in different browsers as well as in both JavaScript-enabled and
JavaScript-disabled environments.

[Source](https://gitlab.com/dklymenk/personal-website)
