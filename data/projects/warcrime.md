Two months into the Russian full-scale invasion of Ukraine I started working on
a war-crime-reporting mobile application for
[Kharkiv Human Rights Protection Group](https://khpg.org/en/). The idea was to
offer volunteers a more convenient alternative to the existing web form.

The iOS and Android applications allow volunteers to:

- upload photos and videos from the device's photo library
- take photos and videos with the device's camera and attach current location to
  them
- fill in a form with description of the event and other information
- upload the form and media files to the server

The admin panel allows the organization's staff to:

- view the list of reports
- view the details of each report
- preview and download the media files attached to reports
- filter the list of reports by date, reporter, and other criteria

[React Native app source](https://github.com/dklymenk/warcrime)\
[NestJS BE and admin panel source](https://github.com/dklymenk/warcrime-api)
