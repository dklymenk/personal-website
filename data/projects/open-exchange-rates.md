[Open Exchange Rates](https://openexchangerates.org/) provides an API for
querying historical exchange rates for various currencies.

[QbilSoftware/OpenExchangeRates](https://github.com/QbilSoftware/OpenExchangeRates)
is an unofficial yet up-to-date library that provides a PHP API client.

I have contributed a minor change that exposes additional functionality of the
API, namely the ability to query rates for cryptocurrencies.

[Contribution](https://github.com/QbilSoftware/OpenExchangeRates/pull/1)
