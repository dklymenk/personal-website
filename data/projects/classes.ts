export const classes = {
  ul: 'list-disc pl-10',
  a: 'transition-colors text-blue-600 hover:text-blue-700 dark:text-blue-500 dark:hover:text-blue-400 font-normal break-words',
};
