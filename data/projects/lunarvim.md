[LunarVim](https://www.lunarvim.org/) is a community-supported Neovim
configuration that ships with sensible defaults and a collection of plugins that
provide a great out-of-the-box experience without sacrificing editor's
extensibility.

I have been using LunarVim since the project started in 2021. I have contributed
a couple of minor changes that were fixing the issues I have experienced myself.

[Contributions](https://github.com/LunarVim/LunarVim/pulls?q=is%3Apr+author%3Adklymenk+is%3Aclosed+)\
[Website](https://www.lunarvim.org/)
