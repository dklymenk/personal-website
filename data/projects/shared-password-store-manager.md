An interactive POSIX-complaint shell script that helps you manage shared
[password stores](https://www.passwordstore.org/). Managing a shared password
store typically requires you to manually edit `.gpg-id` file and hardcraft a
convoluted `pass init` command. This script automates this process and makes it
easy to add/remove users to/from a password store as well as initialise new
stores in subfolders, which allows for a more granular access control.

[Source](https://gitlab.com/dklymenk/shared-password-store-manager)
