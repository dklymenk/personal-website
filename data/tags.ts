import {IconKey} from 'components';

export type TagKey =
  | 'js'
  | 'ts'
  | 'react'
  | 'rn'
  | 'nextjs'
  | 'express'
  | 'nestjs'
  | 'php'
  | 'symfony'
  | 'sh'
  | 'commercial'
  | 'volunteering'
  | 'personal';

export interface Tag {
  key: TagKey;
  label: string;
  iconKey?: IconKey;
  /** If true, only one of Icon or label is rendered. Prioritising Icon. */
  compact?: boolean;
}

const tagsArray: Tag[] = [
  {key: 'js', label: 'JavaScript', iconKey: 'js', compact: true},
  {key: 'ts', label: 'TypeScript', iconKey: 'ts', compact: true},
  {key: 'react', label: 'React', iconKey: 'react'},
  {key: 'rn', label: 'React Native'},
  {key: 'nextjs', label: 'Next.js', iconKey: 'nextjs'},
  {key: 'nestjs', label: 'NestJS', iconKey: 'nestjs'},
  {key: 'php', label: 'PHP', iconKey: 'php'},
  {key: 'symfony', label: 'Symfony', iconKey: 'symfony'},
  {key: 'sh', label: 'Shell Scripting'},
  {key: 'commercial', label: 'Commercial'},
  {key: 'volunteering', label: 'Volunteering'},
  {key: 'personal', label: 'Personal'},
];

const convertTagsArrayToObject = (array: Tag[]) =>
  array.reduce<Record<TagKey, Tag>>(
    (acc, curr) => ((acc[curr.key] = curr), acc),
    <Record<TagKey, Tag>>{}
  );

export const tags = convertTagsArrayToObject(tagsArray);
