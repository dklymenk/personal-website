import {ProjectsList} from 'components';
import {Header} from 'components/projects';
import {getProjects, Project, projectsDisclaimer, TagKey, tags} from 'data';
import {
  GetStaticPaths,
  GetStaticProps,
  InferGetStaticPropsType,
  NextPage,
} from 'next';
import Head from 'next/head';
import {ParsedUrlQuery} from 'querystring';

interface Props {
  activeTagKey?: TagKey;
  projects: Project[];
}

interface Params extends ParsedUrlQuery {
  slug: [TagKey] | [];
}

export const getStaticPaths: GetStaticPaths<Params> = () => {
  // Prepopulate paths with root route
  let paths: {params: Params}[] = [{params: {slug: []}}];

  // Add tag keys to paths
  const tagKeys = Object.keys(tags) as TagKey[];
  tagKeys.forEach((key) => {
    paths.push({
      params: {slug: [key]},
    });
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props, Params> = async ({
  params,
}) => {
  const activeTagKey = params?.slug?.pop();
  const projects = await getProjects();
  const filteredProjects = projects.filter(
    ({tags}) => !activeTagKey || tags?.find(({key}) => activeTagKey === key)
  );

  return {
    props: {...(activeTagKey && {activeTagKey}), projects: filteredProjects},
  };
};

const Projects: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  activeTagKey,
  projects,
}) => {
  return (
    <div>
      <Head>
        <title>Projects | Dmytro Klymenko</title>
        <meta name="description" content={projectsDisclaimer} />
      </Head>
      <Header />
      <ProjectsList
        pagePathname="/projects"
        activeTagKey={activeTagKey}
        projects={projects}
      />
    </div>
  );
};

export default Projects;
