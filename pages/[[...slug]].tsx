import {SectionSeparator} from 'components';
import {
  EducationSection,
  EmploymentHistorySection,
  Header,
  ProjectsSection,
} from 'components/home';
import {
  getProjects,
  Project,
  TagKey,
  tags,
  employmentHistory,
  education,
  tagline,
} from 'data';
import type {
  GetStaticPaths,
  GetStaticProps,
  InferGetStaticPropsType,
  NextPage,
} from 'next';
import Head from 'next/head';
import {ParsedUrlQuery} from 'querystring';

interface Props {
  activeTagKey?: TagKey;
  projects: Project[];
  totalProjects: number;
}

interface Params extends ParsedUrlQuery {
  slug: [TagKey] | [];
}

export const getStaticPaths: GetStaticPaths<Params> = () => {
  // Prepopulate paths with root route
  let paths: {params: Params}[] = [{params: {slug: []}}];

  // Add tag keys to paths
  const tagKeys = Object.keys(tags) as TagKey[];
  tagKeys.forEach((key) => {
    paths.push({
      params: {slug: [key]},
    });
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props, Params> = async ({
  params,
}) => {
  const activeTagKey = params?.slug?.pop();
  const projects = await getProjects();
  const filteredProjects = projects.filter(
    ({tags}) => !activeTagKey || tags?.find(({key}) => activeTagKey === key)
  );
  const totalProjects = filteredProjects.length;

  return {
    props: {
      ...(activeTagKey && {activeTagKey}),
      projects: filteredProjects.slice(0, 3),
      totalProjects,
    },
  };
};

const Home: NextPage<InferGetStaticPropsType<typeof getStaticProps>> = ({
  activeTagKey,
  projects,
  totalProjects,
}) => {
  return (
    <>
      <Head>
        <title>Dmytro Klymenko</title>
        <meta name="description" content={tagline} />
      </Head>

      <Header />

      <ProjectsSection
        activeTagKey={activeTagKey}
        projects={projects}
        totalProjects={totalProjects}
      />

      <SectionSeparator />

      <EmploymentHistorySection employmentHistory={employmentHistory} />

      <SectionSeparator />

      <EducationSection education={education} />
    </>
  );
};

export default Home;
