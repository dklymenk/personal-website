import {PlaywrightTestConfig, devices} from '@playwright/test';

const config: PlaywrightTestConfig = {
  testDir: 'e2e',
  webServer: {
    // Skip the build if SKIP_BUILD is set to 1
    command:
      'if [ ${SKIP_BUILD:-0} -ne "1" ]; then yarn build; fi && yarn start',
    port: 3001,
    reuseExistingServer: !process.env.CI,
    env: {NODE_ENV: 'test', PORT: '3001'},
  },
  projects: [
    {
      name: 'chromium',
      use: {...devices['Desktop Chrome']},
    },
    {
      name: 'dark chromium',
      use: {...devices['Desktop Chrome'], colorScheme: 'dark'},
    },
    {
      name: 'firefox',
      use: {...devices['Desktop Firefox']},
    },
    {
      name: 'noscript chromium',
      use: {...devices['Desktop Chrome'], javaScriptEnabled: false},
    },
    {
      name: 'Pixel 2',
      use: {...devices['Pixel 2']},
    },
    // TODO add webkit for CI only
  ],
};
export default config;
